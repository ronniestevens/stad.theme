from five import grok
from plone.directives import form
from ericka.theme import _
from zope import schema


class IOpdrachtgever(form.Schema):
    """
    """

    external_url = schema.TextLine(
            # url format
            title=_(u"Extern web adres"),
            description=_(u"Web homepage van deze opdrachtgever."),
            required=False, 
        )

class View(grok.View):
    grok.context(IOpdrachtgever)
    grok.require('zope2.View')
    grok.name('view')
