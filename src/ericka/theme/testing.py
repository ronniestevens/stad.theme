from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting

from plone.testing import z2

from zope.configuration import xmlconfig


class ErickathemeLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load ZCML
        import ericka.theme
        xmlconfig.file(
            'configure.zcml',
            ericka.theme,
            context=configurationContext
        )

        # Install products that use an old-style initialize() function
        #z2.installProduct(app, 'Products.PloneFormGen')

#    def tearDownZope(self, app):
#        # Uninstall products installed above
#        z2.uninstallProduct(app, 'Products.PloneFormGen')

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'ericka.theme:default')

ERICKA_THEME_FIXTURE = ErickathemeLayer()
ERICKA_THEME_INTEGRATION_TESTING = IntegrationTesting(
    bases=(ERICKA_THEME_FIXTURE,),
    name="ErickathemeLayer:Integration"
)
