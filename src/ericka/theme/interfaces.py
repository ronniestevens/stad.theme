from zope.interface import Interface
from zope import schema

class IErickaThemeSettings(Interface):

    contactName = schema.TextLine(title=u"Contact name", required=False)
    contactAddress = schema.TextLine(title=u"Contact address", required=False)
    contactEmail = schema.TextLine(title=u"Contact emailaddress", required=False)
    contactPhone = schema.TextLine(title=u"Contact phonenumber", required=False)
    themeColor = schema.TextLine(title=u"Theme Color", default=u"green")
    