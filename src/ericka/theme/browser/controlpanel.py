from plone.app.registry.browser.controlpanel import RegistryEditForm
from plone.app.registry.browser.controlpanel import ControlPanelFormWrapper

from ericka.theme.interfaces import IErickaThemeSettings
from plone.z3cform import layout
from z3c.form import form

class ErickaThemeControlPanelForm(RegistryEditForm):
    form.extends(RegistryEditForm)
    schema = IErickaThemeSettings

ErickaThemeControlPanelView = layout.wrap_form(ErickaThemeControlPanelForm, ControlPanelFormWrapper)
ErickaThemeControlPanelView.label = u"Ericka theme settings"