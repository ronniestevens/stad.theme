# Zope imports
from Acquisition import aq_inner
from zope.interface import Interface
from Products.CMFCore.interfaces import ISiteRoot

from five import grok
from zope.component import getMultiAdapter
from Products.CMFCore.utils import getToolByName


from plone.memoize.instance import memoize

# access the contact settings
from zope.component import getUtility
from plone.registry.interfaces import IRegistry

# Plone imports
from plone.app.layout.viewlets.interfaces import IAboveContent
from plone.app.layout.viewlets.interfaces import IBelowContent
from plone.app.layout.viewlets.interfaces import IPortalTop

from ericka.theme.browser.interfaces import IThemeSpecific


# The viewlets in this file are rendered on every content item type
#grok.context(Interface)

# Use templates directory to search for templates
grok.templatedir('templates')


class contactViewlet(grok.Viewlet):
    """A viewlet showing some site_properties
    """

    grok.context(IThemeSpecific)
    grok.viewletmanager(IPortalTop)

    @property
    def available(self):
        return bool(self.getContact())
        
    def getContact(self):    
        registry = getUtility(IRegistry)
        if registry != 0:
            data = {
                'name' :  registry['ericka.theme.interfaces.IErickaThemeSettings.contactName'],
                'address' : registry['ericka.theme.interfaces.IErickaThemeSettings.contactAddress'],
                'phone' : registry['ericka.theme.interfaces.IErickaThemeSettings.contactPhone'],
                'email' : registry['ericka.theme.interfaces.IErickaThemeSettings.contactEmail'],
            }
                
            return data

    
class NewsFlashViewlet(grok.Viewlet):
    """A viewlet that will show slideshow
    based on the newsflash contenttype
    """
    
    grok.context(ISiteRoot)
    grok.viewletmanager(IAboveContent)

    def available(self):
        if self.newsSlides():
            return True
        else:
            return False

    @memoize
    def newsSlides(self):
            catalog = getToolByName(self.context, 'portal_catalog')
            result = []
            path = self.context.getPhysicalPath()[:-1]
            path = '/'.join(path)
            for newsflash in catalog(
                {'portal_type': 'ericka.theme.newsflash',
                 'path': dict(query=path, depth=2),
                 'sort_on': 'Date',
                 'sort_order': 'descending',
                 }):
                obj = newsflash.getObject()
    
                result.append(
                    dict(url=newsflash.getURL(),
                         title=newsflash.Title,
                         description=newsflash.Description,
                         link=obj.internalLink,
                         linkTitle=obj.internalLinkTitle,
                         image="%s/@@images/image" % obj.absolute_url())
                )
            return result
        


    