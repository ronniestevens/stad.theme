from Products.CMFCore.interfaces import ISiteRoot
from plone.app.contenttypes.interfaces import IFolder
from five import grok
from Products.CMFCore.utils import getToolByName
from plone.memoize.instance import memoize

# needed for the CSSColorClass
from Products.Five.browser import BrowserView
from zope.component import getUtility
from plone.registry.interfaces import IRegistry
from zope.interface import Interface


grok.templatedir('templates')


class ErickaHomepageView(grok.View):
    grok.context(ISiteRoot)


    def homepageProjects(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        result = []
        for project in catalog(
            {'portal_type': 'ericka.theme.project',
             'Subject': 'homepage',
             'sort_on': 'Date',
             'sort_order': 'descending'}
            ):
            obj = project.getObject()
            result.append(
                dict(title=project.Title,
                     url=project.getURL(),
                     description=project.Description,
                     date=project.EffectiveDate,
                     pimage=obj.image,
                     image="%s/@@images/image" % project.getURL())
            )
        return result 

      
    def project_rows(self):
        return self.groupby(self.homepageProjects(), groupsize=3)

    def groupby(self, iterable, groupsize=2):
       
        result = []
        subgroup = []
        for item in iterable:
            subgroup.append(item)
            if len(subgroup) % groupsize == 0:
                result.append(tuple(subgroup))
                subgroup = []
        if len(subgroup) > 0:
            result.append(tuple(subgroup))
        return result
    

class ProjectsView(grok.View):
    grok.context(Interface)


    def projects(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        result = []
        for project in catalog(
            {'portal_type': 'ericka.theme.project',
             'sort_on':'getObjPositionInParent',
             'sort_order': 'descending',}
            ):
            obj = project.getObject()
            result.append(
                dict(title=project.Title,
                     url=project.getURL(),
                     description=project.Description,
                     startyear=obj.start,
                     endyear=obj.einde,
                     ext_url=obj.external_url,
                     opdrachtgever=obj.klant,
                     contact1=obj.contactpersoon1,
                     contact2=obj.contactpersoon2,
                     pimage=obj.image,
                     archief=obj.archief,
                     image="%s/@@images/image" % project.getURL())
            )
        return result 

      
    def project_rows(self):
        return self.groupby(self.projects(), groupsize=3)

    def groupby(self, iterable, groupsize=2):
       
        result = []
        subgroup = []
        for item in iterable:
            subgroup.append(item)
            if len(subgroup) % groupsize == 0:
                result.append(tuple(subgroup))
                subgroup = []
        if len(subgroup) > 0:
            result.append(tuple(subgroup))
        return result


class pageListing(grok.View):
    grok.context(IFolder)
    
    def pages(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        result = []
        for page in catalog(
            {'portal_type': 'Document',
             'sort_on': 'getObjPositionInParent',
             'sort_order': 'descending',}
            ):
            obj = page.getObject()
            result.append(
                dict(title=page.Title,
                     url=page.getURL(),
                     description=obj.description)
            )
        return result
    
    
    
class colorView(grok.View):
    
    grok.context(Interface)
    
    def getColor(self):
        registry = getUtility(IRegistry)
        if registry != 0:
            color = registry['ericka.theme.interfaces.IErickaThemeSettings.themeColor']
            
        return color


class NestedFolderView(grok.View):
    """Nested Folder View showing
    the opdrachtgevers
    """
    
    grok.context(Interface)

    def get_folders(self, item):
        """Returns the folderish items which are contained in item"""
        if item.portal_type == 'Topic':
            return item.queryCatalog()

        if hasattr(item, 'getPath'):
            # item is a brain
            path = item.getPath()
        else:
            # item is an object
            path = '/'.join(item.getPhysicalPath())
        catalog = getToolByName(self.context, "portal_catalog")
        brains = catalog(path={"query": path, "depth": 1},
                         sort_on='getObjPositionInParent',
                         is_folderish=True)
        klanten = catalog(path={"query": path, "depth": 1},
                               sort_on='getObjPositionInParent',
                               portal_type="ericka.theme.opdrachtgever")

        if klanten:
            brains = brains + klanten
        return brains

    def get_twos(self, folders=[]):
        """Group the folderish items in sets of two."""
        return group_twos(list(folders))

    def get_threes(self, folders=[]):
        """Group the folderish items in sets of three."""
        return group_threes(list(folders))


def group_twos(singles):
    twos = []
    while singles:
        a = singles.pop(0)
        try:
            b = singles.pop(0)
            twos.append((a, b))
        except IndexError:
            twos.append((a,))
    return twos


def group_threes(singles):
    threes = []
    while singles:
        row = [singles.pop(0)]
        try:
            row.append(singles.pop(0))
            row.append(singles.pop(0))
        except IndexError:
            pass
        threes.append(row)
    return threes