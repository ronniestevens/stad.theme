$('.carousel').carousel()

/* Flex Slider */
$(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: true,
        pauseOnHover: true,
        slideshowSpeed: 5000
    });
});

jQuery(".prettyphoto").prettyPhoto({
    overlay_gallery: false, social_tools: false
});
