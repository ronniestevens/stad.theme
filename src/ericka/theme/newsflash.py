from five import grok
from plone.directives import form

from ericka.theme import _

from plone.app.textfield import RichText
from plone.namedfile.interfaces import IImageScaleTraversable
from plone.namedfile.field import NamedBlobImage
from zope import schema
from z3c.relationfield.schema import RelationChoice
from plone.formwidget.contenttree import ObjPathSourceBinder


class INewsFlash(form.Schema, IImageScaleTraversable):
    """A news flash for the homepage
       which renderes as a slideshow
    """

    body = RichText(
        title=_(u"Description"),
        required=False,)

    image = NamedBlobImage(
        title=_(u"Image"),
        description=_(u"940x300 pixels"),
        required=False,)

    internalLink = RelationChoice(
        title=_(u"Internal Link"),
        source=ObjPathSourceBinder(),
        required=False,)

    internalLinkTitle = schema.TextLine(
        title=_(u"Internal link title"),
        description=_(u"Visible title of link"),
        required=False,)


class View(grok.View):
    grok.context(INewsFlash)
    grok.require('zope2.View')
    grok.name('view')
