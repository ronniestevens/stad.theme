from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary
from Products.CMFCore.utils import getToolByName
from five import grok


@grok.provider(IContextSourceBinder)
def opdrachtgevers(context):
    catalog = getToolByName(context, 'portal_catalog')
    terms = []
    for brain in catalog(
      {'portal_type': 'ericka.theme.opdrachtgever',}):
      name = brain.Title
      id = brain.getId
      terms.append(SimpleVocabulary.createTerm(id, str(id), name))
      
 
    return SimpleVocabulary(terms)