from five import grok
from plone.directives import form

from Products.CMFCore.utils import getToolByName


from ericka.theme import _

from plone.app.textfield import RichText
from plone.namedfile.interfaces import IImageScaleTraversable
from plone.namedfile.field import NamedBlobImage, NamedBlobFile
from zope import schema
from z3c.relationfield.schema import RelationChoice, RelationList
from plone.formwidget.contenttree import ObjPathSourceBinder

from collections import OrderedDict
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

#from ericka.theme import vocab
from ericka.theme import opdrachtgever

pdates = OrderedDict([('1999', _(u"1999")),
                        ('2000', _(u"2000")),
                        ('2001', _(u"2001")),
                        ('2002', _(u"2002")),
                        ('2003', _(u"2003")),
                        ('2004', _(u"2004")),
                        ('2005', _(u"2005")),
                        ('2006', _(u"2006")),
                        ('2007', _(u"2007")),
                        ('2008', _(u"2008")),
                        ('2009', _(u"2009")),
                        ('2010', _(u"2010")),
                        ('2011', _(u"2011")),
                        ('2012', _(u"2012")),
                        ('2013', _(u"2013")),
                        ('2014', _(u"2014")),
                        ('2015', _(u"2015")),
                        ('2016', _(u"2016")),
                        ('2017', _(u"2017")),
                        ('2018', _(u"2018")),
                        ('2019', _(u"2019")),
                        ('2020', _(u"2020")),
                        ('heden', _(u"heden"))
                        ])
              

pdates_vocabulary = SimpleVocabulary(
    [
        SimpleTerm(value=k, token=k, title=v)
        for (k, v) in pdates.items()
    ]
)


class IProject(form.Schema, IImageScaleTraversable):
    """A news flash for the homepage
       which renderes as a slideshow
    """

    body = RichText(
        title=_(u"Description"),
        required=False,)

    image = NamedBlobImage(
        title=_(u"Image"),
        description=_(u"940x300 pixels"),
        required=False,)

   # opdrachtgever = schema.Choice(
   #     source = vocab.opdrachtgevers,
   #     title=_(u"Opdrachtgever"),
   #     required=False,
   #     missing_value='',)
   
    external_url = schema.TextLine(
            # url format
            title=_(u"Extern web adres"),
            description=_(u"Web homepage van dit project."),
            required=False, 
        )

    klant = RelationList(
        title=_(u"klanten"),
        default=[],
        value_type=RelationChoice(title=_(u"Klant"),
                                  source=ObjPathSourceBinder()),
        required=False,)

    contactpersoon1 = schema.TextLine(
        title=_(u"Contactpersoon"),
        required=False,)
    
    contactpersoon2 = schema.TextLine(
        title=_(u"Contactpersoon"),
        required=False,)

    start = schema.Choice(
        vocabulary=pdates_vocabulary,
        title=_(u"Start"),
        description=_(u"Start van project"),
        required=True,
        missing_value='')
    
    einde = schema.Choice(
        vocabulary=pdates_vocabulary,
        title=_(u"Einde"),
        description=_(u"Einde van project"),
        required=False,
        missing_value='')
    
    archief = schema.Bool(
        title=_(u"archief"),
        required=False,
    )

    bestand = NamedBlobFile(
        title=_(u"Bijlage"),
        description=_(u"PDF upload"),
        required=False,
    )

class View(grok.View):
    grok.context(IProject)
    grok.require('zope2.View')
    grok.name('view')

#    def opdrachtgever(self):
#        """Getting the title of
#        the opdrachtgever
#       """
#        myvocab = vocab.opdrachtgevers(self)
#        return myvocab.getTerm(self.context.opdrachtgever).title
    
    def startyear(self):
        startyear = str(self.context.start)
        startyear = startyear[0:4]
        return startyear
    
    def endyear(self):
        endyear = str(self.context.einde)
        endyear = endyear[0:4]
        return endyear
    
    def projectimages(self):

        catalog = getToolByName(self.context, 'portal_catalog')
        result = []
        path = '/'.join(self.context.getPhysicalPath())
        for projectimage in catalog(
            {'portal_type': 'Image',
             'path': dict(query=path, depth=1),
             'sort_on': 'getObjPositionInParent',
             'sort_order': 'descending',
             }):
            obj = projectimage.getObject()
            result.append(
                dict(title=projectimage.Title,
                     description=obj.Description,
                     image="%s/@@images/image/medium" % obj.absolute_url()))
        return result
    
    